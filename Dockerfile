# This file is a template, and might need editing before it works on your project.
FROM python

WORKDIR /usr/src/app

#COPY requirements.txt /usr/src/app/

RUN pip install --no-cache-dir numpy

COPY . /usr/src/app

EXPOSE 8080

CMD ["python", "start_server.py", "8080", "20", "2"]
